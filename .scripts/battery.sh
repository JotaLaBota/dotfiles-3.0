#!/bin/bash
### BATTERY ###
battery_percentage=$(acpi | awk '{print $4}' | sed "s/,//")
battery_value=$(echo $battery_percentage | sed "s/%//")
if [ $battery_value -lt 10 ]; then
	battery_icon="   "	
elif [ $battery_value -lt 25 ]; then
	battery_icon="   "	
elif [ $battery_value -lt 50 ]; then
	battery_icon="   "	
elif [ $battery_value -lt 75 ]; then
	battery_icon="   "	
else
	battery_icon="   "	
fi

battery="$battery_icon$battery_percentage"

### PRINT ###
printf "%s\n" "$battery"

# Icons
# 󰕿
# 󰖀
# 󰕾
# 󰸈
# 
# 
# 
# 
# 
#
# 󰹐
# 󱩎
# 󱩏
# 󱩐
# 󱩑
# 󱩒
# 󱩓
# 󱩔
# 󱩕
# 󱩖
# 󰛨
