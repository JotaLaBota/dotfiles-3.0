r=$(find ~/ -type f -iname "*.pdf" | sed 's:.*/::' | rofi -dmenu -i -p "Select pdf: ")
s=$(find -path "*$r" | sed '1!d')
case "$s" in
  	".") ;;
	*) echo $s; zathura "$s" 2>/dev/null;;
esac
