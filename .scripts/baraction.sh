while :; do
	### VOLUME ###
	volume_value=$(pamixer --get-volume)
	if [ "$(pamixer --get-mute)" = false ]; then
		if [ $volume_value -lt 33 ]; then
			volume_icon="󰕿 "
		elif [ $volume_value -lt 66 ]; then
			volume_icon="󰖀 "
		else
			volume_icon="󰕾 "
		fi
		volume="$volume_icon$volume_value"
	else
    	    volume="󰖁 $volume_value"
	fi

	### BATTERY ###
	battery_percentage=$(acpi | awk '{print $4}' | sed "s/,//")
	battery_value=$(echo $battery_percentage | sed "s/%//")
	if [ $battery_value -lt 10 ]; then
		battery_icon="  "	
	elif [ $battery_value -lt 25 ]; then
		battery_icon="  "	
	elif [ $battery_value -lt 50 ]; then
		battery_icon="  "	
	elif [ $battery_value -lt 75 ]; then
		battery_icon="  "	
	else
		battery_icon="  "	
	fi

	battery="$battery_icon$battery_percentage"

	### BRIGHTNESS ###
	brightness_decimal=$(brillo)
	brightness_value=$(echo ${brightness_decimal%.*})
	if [ $brightness_value -lt 10 ]; then
		brightness_icon="󱩎 "	
	elif [ $brightness_value -lt 20 ]; then
		brightness_icon="󱩏 "	
	elif [ $brightness_value -lt 30 ]; then
		brightness_icon="󱩐 "	
	elif [ $brightness_value -lt 40 ]; then
		brightness_icon="󱩑 "	
	elif [ $brightness_value -lt 50 ]; then
		brightness_icon="󱩒 "	
	elif [ $brightness_value -lt 60 ]; then
		brightness_icon="󱩓 "	
	elif [ $brightness_value -lt 70 ]; then
		brightness_icon="󱩔 "	
	elif [ $brightness_value -lt 80 ]; then
		brightness_icon="󱩕 "	
	elif [ $brightness_value -lt 90 ]; then
		brightness_icon="󱩖 "	
	else
		brightness_icon="󰛨 "	
	fi

	brightness="$brightness_icon$brightness_value"

	### PRINT ###
	printf "%s %s %s\n" "$brightness" "$volume" "$battery"
	sleep .5
done

# Icons
# 󰕿
# 󰖀
# 󰕾
# 󰸈
# 
# 
# 
# 
# 
#
# 󰹐
# 󱩎
# 󱩏
# 󱩐
# 󱩑
# 󱩒
# 󱩓
# 󱩔
# 󱩕
# 󱩖
# 󰛨
