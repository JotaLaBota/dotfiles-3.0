#!/bin/bash
### BRIGHTNESS ###
brightness_decimal=$(brillo)
brightness_value=$(echo ${brightness_decimal%.*})

if [ $brightness_value -lt 10 ]; then
	brightness_icon="󱩎  "	
elif [ $brightness_value -lt 20 ]; then
	brightness_icon="󱩏  "	
elif [ $brightness_value -lt 30 ]; then
	brightness_icon="󱩐  "	
elif [ $brightness_value -lt 40 ]; then
	brightness_icon="󱩑  "	
elif [ $brightness_value -lt 50 ]; then
	brightness_icon="󱩒  "	
elif [ $brightness_value -lt 60 ]; then
	brightness_icon="󱩓  "	
elif [ $brightness_value -lt 70 ]; then
	brightness_icon="󱩔  "	
elif [ $brightness_value -lt 80 ]; then
	brightness_icon="󱩕  "	
elif [ $brightness_value -lt 90 ]; then
	brightness_icon="󱩖  "	
else
	brightness_icon="󰛨  "	
fi

brightness="$brightness_icon$brightness_value"

### PRINT ###
printf "%s\n" "$brightness"

# Icons
# 󰕿
# 󰖀
# 󰕾
# 󰸈
# 
# 
# 
# 
# 
#
# 󰹐
# 󱩎
# 󱩏
# 󱩐
# 󱩑
# 󱩒
# 󱩓
# 󱩔
# 󱩕
# 󱩖
# 󰛨
