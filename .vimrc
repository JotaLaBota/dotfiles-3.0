set relativenumber
set hlsearch
set ruler
set foldlevel=99
" set cursorline

set expandtab
set tabstop=4
set shiftwidth=4

set autoindent

" Make searches case-insensitive unless capitals are used
set ignorecase
set smartcase

" Show partial commands in the bottom right
set showcmd

"set winheight=35

" Keep a few lines visible above/below cursor when scrolling
set scrolloff=5


" set clipboard=unnamedplus

"Needed for polyglot
set nocompatible

let mapleader = "ñ"
let g:vimwiki_folding = 'syntax'

call plug#begin()
	Plug 'vimwiki/vimwiki'
	Plug 'preservim/nerdtree'
    Plug 'dylanaraps/wal.vim'
    Plug 'junegunn/fzf'
    Plug 'junegunn/fzf.vim'
    Plug 'sheerun/vim-polyglot'
    Plug 'SirVer/ultisnips'
    "Plug 'lifepillar/vim-mucomplete'
    "Plug 'ycm-core/YouCompleteMe'
    "Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

" Reload .vimrc without restarting Vim
nnoremap <leader>r :source ~/.vimrc<CR>

nnoremap <silent> <leader><leader> :noh<cr><esc>
nnoremap <leader>y "+y
nnoremap <leader>p "+p
nnoremap <leader>n :NERDTreeToggle<CR>

"FZF config
nnoremap <leader>ff :Files<CR>
nnoremap <leader>fl :Rg<CR>
nnoremap <leader>fb :Buffers<CR>

nnoremap <C-k> :bn<CR>
nnoremap <C-j> :bp<CR>
nnoremap <leader>cb :bd<CR>


"Open NERDTree automatically and close vim even if it's still open
"autocmd VimEnter * NERDTree
autocmd BufEnter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif


colorscheme wal

if has('gui_running')
  au VimEnter * colorscheme habamax
endif


"These were for mucomplete
"let g:mucomplete#enable_auto_at_startup = 1
"let g:mucomplete#chains = {
"\ 'default': ['path', 'omni', 'keyn', 'dict', 'uspl'],
"\ }
"set shortmess+=c
"set completeopt=menuone,noinsert,noselect



"And these were for coc
" Use Tab to trigger and navigate completion suggestions
"inoremap <silent><expr> <C-j> pumvisible() ? "\<C-n>" : "\<C-j>"
"inoremap <silent><expr> <C-k> pumvisible() ? "\<C-p>" : "\<C-k>"

" Use Enter to confirm completion
"inoremap <expr> <Tab> pumvisible() ? "\<C-y>" : "\<CR>"

"Script to toggle terminal
let t:is_terminal_active = 0
function ToggleTerm()
    if t:is_terminal_active == 0
        belowright terminal
        let t:is_terminal_active = 1
        let t:buf_num = bufnr('%')
        wincmd p
    else
        execute 'bd!' t:buf_num
        let t:is_terminal_active = 0
    endif
endfunction

nnoremap <leader>t :call ToggleTerm()<CR>

autocmd TerminalWinOpen *
  \ if &buftype == 'terminal' |
  \   resize 10 |
  \   setlocal termwinsize=0x140 |
  \   setlocal nowrap |
  \ endif

" Manually set the status line color.
hi StatusLine ctermbg=15 ctermfg=0
hi StatusLineNC ctermbg=1 ctermfg=0
hi StatusLineTerm ctermbg=0 ctermfg=15
hi StatusLineTermNC ctermbg=0 ctermfg=1
