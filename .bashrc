### EXPORT ###
export EDITOR='vim'
export VISUAL='vim'
export HISTCONTROL=ignoreboth:erasedups
export PAGER='less'
export PATH="/home/jota/.local/bin:$PATH"
export PS1='\[\e[36m\]\u  \[\e[0m\] ' #30-37 90-97 #  


#list
#alias ls='ls --color=auto'

## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'

#readable output
alias df='df -h'

set -o vi

# Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.
# Not supported in the "fish" shell.
#(cat ~/.cache/wal/sequences &)
#(sed 's/\[95\]//g' ~/.cache/wal/sequences &)

if [ -n "$TMUX" ]; then
    echo ''
else
    (cat ~/.cache/wal/sequences &)
fi

neofetch


alias fcd='dir=$(find ~ -type d 2>/dev/null | fzf) && cd "$dir"'
alias lfcd='cd $(lf -print-last-dir)'







# LS_COLORS = 'di=1;4;12;31:ln=1;12'
# 1 número = bold,underline,etc.
# 	0 normal, 1 bold, 2 dim, 4 underline, 5 blinking, 7 reversed, 8 hidden
# 2 números  =  color
# di = directories
# ln = symbolic link
# so = socket
# pi = pipe
# ex = executable
# bd = block special
# cd = character special
# su = setuid executable
# sg = setgid executable
# tw = directory writable to others with sticky bit
# ow = directory writable to others without sticky bit
# or = orphan symlink
# mi = missing file that an orphan symlink points to
# *.extension 


#function pomo() {
#    arg1=$1
#    shift
#    args="$*"
#
#    min=${arg1:?Example: pomo 15 Take a break}
#    sec=$((min * 60))
#    msg="${args:?Example: pomo 15 Take a break}"
#
#    while true; do
#        date '+%H:%M' && sleep "${sec:?}" && notify-send -u critical -t 0 -a pomo "${msg:?}"
#    done
#}
